# T2679_InicioReadDB.R
library(readr)
library(dplyr)
library(stringr)

setwd("~/apgyeJusEROrganization/inst/apgye")
entidades <- read_csv("entidades.csv", show_col_types = FALSE)

soloParana <- c("Juzgado de Familia N°")
solo_PenalNiños_Pna <- c("Juzgado Penal de Niños, Niñas y Adolescentes (NNyA)")
solo_Familia_y_PenalNiños <- c("Juzgado de Familia y Penal de Niños, Niñas y Adolescentes (NNyA)")

# ver cuantos hay en la Pcia (24 con Paraná completo)
# [1] "jdofam0000cha" "jdofam0000dia" "jdofam0000fel" "jdofam0000frl" "jdofam0000gay" "jdofam0000lpa"
# [7] "jdofam0000nog" "jdofam0000tal" "jdofam0000vic" "jdofam0000vil" "jdofam0001col" "jdofam0100uru"
# [13] "jdofam0200uru" "jdofam0002col" "jdofam0100pna" "jdofam0200pna" "jdofam0300pna" "jdofam0400pna"
# [19] "jdofam0100gch" "jdofam0200gch" "jdofam0100con" "jdofam0200con" "jdofam0300con" "jdofam0500pna"

# observar <- entidades %>% 
#   filter(tipo == "jdo" &
#            grepl("fam|Fam|Pen|Ado", organismo_descripcion) & 
#            !grepl("sec|Sec|Familia|Civ", organismo_descripcion) &
#            !organismo_descripcion == "Jdo Fam",
#            grepl("fam|pen", materia)) # estos cambiar y seguir
# 
# table(observar$organismo_descripcion)
# View(observar) # esto me dan 15 organismos (sin parana)


# source("~/apgyeinformes/R/poblacion.R")
# jdos_familia %>% View()
# 
# pres_filtrado <- presentaciones_justat %>% 
#   filter(grepl("fam", iep))
# 
# View(pres_filtrado)
# unique(pres_filtrado$iep)

entidades_cambiado <- entidades %>% 
  mutate(organismo_descripcion = 
         case_when( 
           localidad == "Paraná" &
             tipo == "jdo" &
             materia == "fam" ~ str_replace(organismo_descripcion, "Jdo Familia", soloParana),
           organismo == "jdopna0000pna" ~ solo_PenalNiños_Pna,
           tipo == "jdo" &
             grepl("fam|Fam|Pen|Ado", organismo_descripcion) & 
             !grepl("sec|Sec|Familia|Civ", organismo_descripcion) &
             !organismo_descripcion == "Jdo Fam" &
             grepl("fam|pen", materia) ~ str_replace(organismo_descripcion, "Jdo Fam Pen Niñ Ado", solo_Familia_y_PenalNiños),
           organismo %in% c("jdofam0001col", "jdofam0002col") ~ str_replace(organismo_descripcion, "Jdo Fam Pen N.A.", solo_Familia_y_PenalNiños),
           TRUE ~ organismo_descripcion
        ))

# View(entidades_cambiado)


# a mano, no lo reconoce el case_when
 entidades_cambiado$organismo_descripcion[entidades_cambiado$organismo == "jdofam0000fel"] <- solo_Familia_y_PenalNiños
 entidades_cambiado$organismo_descripcion[entidades_cambiado$organismo == "jdofam0000frl"] <- solo_Familia_y_PenalNiños
 entidades_cambiado$organismo_descripcion[entidades_cambiado$organismo == "jdofam0000gay"] <- solo_Familia_y_PenalNiños
 entidades_cambiado$organismo_descripcion[entidades_cambiado$organismo == "jdofam0000vil"] <- solo_Familia_y_PenalNiños
 entidades_cambiado$organismo_descripcion[entidades_cambiado$organismo == "jdofam0000nog"] <- solo_Familia_y_PenalNiños
 


write.table(entidades_cambiado, "entidades.csv", sep = ",", row.names = F, col.names = T)
