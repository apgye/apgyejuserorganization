# Actualizacion
library(dplyr)

# Elimino operación mediación vieja y agrego nueva
entidad_operacion <- entidad_operacion[!entidad_operacion$operacion == "MEDIACC", ] 

op_cemarc <- entidad_operacion %>% 
  filter(organismo == "cencco0000pna") %>% 
  select(operacion, operacion_descripcion)

op_pen <- op_cemarc
op_pen$operacion <- c("AUDIFJ", "GTIA_VS", "TRAN_VS", "TJUI_VS")
op_pen$operacion_descripcion <- c("Audiencias Fijadas", 
                                  "Anexo Garantías", 
                                  "Anexo Transición", 
                                  "Anexo Tribunal de Juicio")

operacion_descripcion <- bind_rows(operacion_descripcion, op_cemarc, op_pen)
write.table(entidad_operacion, "entidad_operacion.csv", 
            col.names = T, row.names = F, sep = ",")
write.table(operacion_descripcion, "operacion_descripcion.csv", 
            col.names = T, row.names = F, sep = ",")
