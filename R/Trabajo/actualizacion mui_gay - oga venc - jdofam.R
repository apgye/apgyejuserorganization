# Actualizcion mui, oga vencimiento 15 y jdo familia col
# MUI
entidad_operacion <- entidad_operacion[!(entidad_operacion$organismo == "muistj0400gay" &
                    entidad_operacion$operacion == "CITIPM2"), ]

# OGA vencimiento
entidad_operacion$fvencimiento[entidad_operacion$organismo == "ogapen0000pna"] <- 15

# jdofam0000col
entidad_operacion$comienzo[entidad_operacion$organismo %in% c("jdofam0002col", 
                                                     "jdofam0001col") &
                    !(entidad_operacion$operacion %in% c("CITIPJ", 
                                                       "CETAL"))] <- "2017-12-01"

# corregir oga AUDFIJ
entidad_operacion$comienzo[entidad_operacion$organismo == "ogapen0000pna" &
                             entidad_operacion$operacion == "AUDIFJ"] <- "2018-02-01"


# corregir mui gchu m2

entidad_operacion <- entidad_operacion[!(entidad_operacion$organismo == "muistj0300gch" &
                                           entidad_operacion$operacion == "CITIPM2"), ]


write.table(entidad_operacion, "entidad_operacion.csv", 
            col.names = T, row.names = F, sep = ",") 



