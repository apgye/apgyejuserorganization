# Df Operaciones por Organismo

# Build a df froma scratch
library(dplyr)
library(tibble)
library(stringr)
conectar_base_justat()


# Definir operaciones por órgano
# OxO = f(x) = fuero + tipo + materia
operaciones <- listar_operaciones()
str(entidades)

# df de almacenamiento
entidad_operacion <- tribble(
  ~organismo, ~id_operacion, ~periodicidad, ~finicio, ~auxiliares, ~ffin
)

# Código auxiliares
"3" = "campos cco" # causas iniciadas ppal, causas inic. no ppal, causas archivadas
"6" = "campos cco y lab" # idem x2
"9" = "campos cco, fam, lab" # idem x3
"91" = "campos cco, lab, pen" # idem x3
"1" = "causas archivadas" # causas archivadas
"10" = "auxiliares específicos penales viejo sistema"

# funcion de trabajo
asignOp <- function(fuero, tipo_org, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}


############################## ASIGNACIONES #############################

# asignar CADRIA1C
asignOp("Civil", "jdo", "cco", "CADRIA1C", "mensual", "2017-11-01", 3, NA)
asignOp("Civil", "jdo", "fam", "CADRIA1C", "mensual", "2017-11-01", 3, NA)
asignOp("Civil", "jdo", "fam|pen", "CADRIA1C", "mensual", "2017-11-01", 3, NA)

# asignar CARDIA1C a paz 1 ----------------------------------------------------
asignOp <- function(fuero, tipo_org, cat, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             categoria == cat &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidades_op$categoria <- NULL
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

asignOp("Civil", "jdo", 1, "paz", "CADRIA1C", "mensual", "2017-11-01", 3, NA)
rm(asignOp)



# rehabilito funcion 
asignOp <- function(fuero, tipo_org, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

# asignar CADRIA1CM con 6 campos auxiliares
asignOp("Civil", "jdo", "cco|lab", "CADRIA1CM", "mensual", "2017-11-01", 6, NA)

#------------------------------------------------------------------------------
# Corrección de asignación de materia en entidades a jdocco0000frl
# entidades$materia[entidades$organismo == "jdocco0000frl"] <- "cco|lab"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar CADRIA1CM2 
asignOp("Civil", "jdo", "cco|fam|lab", "CADRIA1CM2", "mensual", "2017-11-01", 9, NA)

#------------------------------------------------------------------------------
# Corrección de asignación de materia en entidades a jdocco0000frl
#entidades$materia[entidades$organismo == "jdopen0000fel"] <- "cco|lab|pen"
#entidades$materia[entidades$organismo == "jdofam0000fel"] <- "fam"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar CADRIA1CM3
asignOp("Civil", "jdo", "cco|lab|pen", "CADRIA1CM3", "mensual", "2017-11-01", 91, NA)

# asignar CADUR1C
asignOp("Civil", "jdo", "cco", "CADUR1C", "anual", "2018-10-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab", "CADUR1C", "anual", "2018-10-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "CADUR1C", "anual", "2018-10-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab|pen", "CADUR1C", "anual", "2018-10-01", 0, NA)


# asignar CADRIA2C
asignOp("Civil", "cam", "cco", "CADRIA2C", "mensual", "2017-11-01", 0, NA)

# asignar CADUR2C
asignOp("Civil", "cam", "cco", "CADUR2C", "anual", "2018-10-01", 0, NA)

# Operaciones AUDI-------------------------------------------------------------
# reestructuración: que cada órgano tenga acceso a los registros y listados de 
# la operación sobre audencia según la materia correspondiente. Eso permitirá tener 
# autonomía de operación, registro y listado por materia.
#------------------------------------------------------------------------------

# asignar AUDIC 
asignOp("Civil", "jdo", "cco", "AUDIC", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab", "AUDIC", "mensual", "2017-11-01", 0, "2018-04-01")
asignOp("Civil", "jdo", "cco|fam|lab", "AUDIC", "mensual", "2017-11-01", 0, "2018-04-01")
asignOp("Civil", "jdo", "cco|lab|pen", "AUDIC", "mensual", "2017-11-01", 0, "2018-04-01")

#------------------------------------------------------------------------------
# Corrección de asignación de materia en entidades a para familia
# incorporando descripción de materia "pen"
#entidades$materia[entidades$organismo == "jdofam0000fel"] <- "fam|pen"
#entidades$materia[entidades$organismo == "jdofam0000frl"] <- "fam|pen"
#entidades$materia[entidades$organismo == "jdofam0000gay"] <- "fam|pen"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar AUDIF familia y con competencia fam
asignOp("Civil", "jdo", "fam", "AUDIF", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "fam|pen", "AUDIF", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "AUDIF", "mensual", "2017-11-01", 0, "2018-04-01")

#------------------------------------------------------------------------------
# Corrección de asignación del juzgado 6 civil y comercial concordia (ex laboral)
#entidades$fuero[entidades$organismo == "jdocco0600con"] <- "Civil y Comercial"
#entidades$organismo_descripcion[entidades$organismo == "jdocco0600con"] <- 
#  "Jdo Civ y Com 6"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar AUDIL
asignOp("Laboral", "jdo", "lab", "AUDIL", "mensual", "2017-11-01", 0, NA)

# a Partir de mayo los procesos sobre audiencias (AUDI*) trabajan con registros 
# y listados separados según cada materia. 

# asignar AUDIC 
asignOp("Civil", "jdo", "cco|lab", "AUDIC", "mensual", "2018-05-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "AUDIC", "mensual", "2018-05-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab|pen", "AUDIC", "mensual", "2018-05-01", 0, NA)

# asignar AUDIF
asignOp("Civil", "jdo", "cco|fam|lab", "AUDIF", "mensual", "2018-05-01", 0, NA)

# asignar AUDIL
asignOp("Civil", "jdo", "cco|lab", "AUDIL", "mensual", "2018-05-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "AUDIL", "mensual", "2018-05-01", 0, NA)

#------------------------------------------------------------------------------

# asignar MEDIACC
asignOp("Civil", "cen", "cco", "MEDIACC", "mensual", "2018-04-01", 0, NA)

# asignar CADUR3C
asignOp("Civil", "sal", "cco", "CADUR3C", "anual", "2018-10-01", 0, NA)

# asignar CADRIA3C
asignOp("Civil", "sal", "cco", "CADRIA3C", "mensual", "2017-11-01", 0, NA)

# asignar ETIS
asignOp("Civil", "ein", "cco", "ETIS", "mensual", NA, 0, NA)
asignOp("Penal", "ein", "pen", "ETIS", "mensual", NA, 0, NA)

# --------------------------------------------------------------------------
# Operaciones Institucionales del APGE
# "INSTGRAL"
# "INSTPENAL"
#---------------------------------------------------------------------------

# asignar CADRIA1L
asignOp("Laboral", "jdo", "lab", "CADRIA1L", "mensual", "2018-04-01", 3, NA)

# asignar CADUR1L
asignOp("Laboral", "jdo", "lab", "CADUR1L", "anual", "2018-10-01", 0, NA)


# asignar CADRIA2L
asignOp("Laboral", "cam", "lab", "CADRIA2L", "mensual", "2018-04-01", 0, NA)

# asignar CADUR2L
asignOp("Laboral", "cam", "lab", "CADUR2C", "anual", "2018-10-01", 0, NA)


#----------------------------------------------------------------------------
# Posibilidad de operaciones opcionales
#----------------------------------------------------------------------------

# asignar CADRIA3L
asignOp("Laboral", "sal", "lab", "CADRIA3L", "mensual", "2018-04-01", 0, NA)

# asignar CADUR3L
asignOp("Laboral", "sal", "lab", "CADUR3L", "anual", "2018-10-01", 0, NA)

# asignar AUDIP sin fecha inicio
asignOp("Penal", "oga", "pen", "AUDIP", "mensual", NA, 0, NA)

# asigna AUDIP con fecha OGA Pna
#entidad_operacion$finicio[entidad_operacion$organismo == "ogapen0000pna"] <-
#  "2017-11-01"

# asignar CADRC causas a despacho resueltas casacion
asignOp("Penal", "cam", "pen", "CADRC", "mensual", "2017-12-01", 10, NA)

# asignar MEDIACP 
asignOp("Penal", "oma", "pen", "MEDIACP", "mensual", NA, 0, NA)

# asignar individual a OGA pna con fecha---------------------------------------
# oga_pna <- tribble(
#   ~organismo, ~id_operacion, ~periodicidad, ~finicio, ~auxiliares, ~ffin,
#   "ogapen0000pna", "RESOLPG", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "RESOLPA", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "RESOLPT", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "CIGTIA", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "CIJUI", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "CIAP", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "AUDIFJ", "mensual", "2017-11-01", 0, NA, 
#   "ogapen0000pna", "ALLI", "mensual", "2017-11-01", 0, NA
# )
# entidad_operacion <- bind_rows(entidad_operacion, oga_pna)

# asignar CADRS
asignOp("Penal", "sal", "pen|ppc", "CADRS", "mensual", NA, 0, NA)

# agregar operacion CADRS_CONST para procedimientos constitucionales----------

# asignar CITIPJ
asignOp("Civil", "jdo", "cco", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab|pen", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "fam", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "fam|pen", "CITIPJ", "semestral", "2018-02-01", 0, NA)
asignOp("Laboral", "jdo", "lab", "CITIPJ", "semestral", "2018-02-01", 0, NA)

# asignar CITIPJ a paz 1 ----------------------------------------------------
asignOp <- function(fuero, tipo_org, cat, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             categoria == cat &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidades_op$categoria <- NULL
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

asignOp("Civil", "jdo", 1, "paz", "CITIPJ", "semestral", "2018-02-01", 0, NA)
rm(asignOp)

# Rehabilito Función
asignOp <- function(fuero, tipo_org, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

asignOp("Civil", "cam", "cco", "CITIPJ", "semestral", "2017-11-01", 0, NA)
asignOp("Laboral", "cam", "lab", "CITIPJ", "semestral", "2017-11-01", 0, NA)

# asignar CAMOV
asignOp("Civil", "jdo", "cco", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab|pen", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "fam", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", "fam|pen", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Laboral", "jdo", "lab", "CAMOV", "mensual", "2018-04-01", 0, NA)

asignOp("Civil", "cam", "cco", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Laboral", "cam", "lab", "CAMOV", "mensual", "2018-04-01", 0, NA)

asignOp("Civil", "sal", "cco", "CAMOV", "mensual", "2017-11-01", 0, NA)
asignOp("Laboral", "sal", "lab", "CAMOV", "mensual", "2017-11-01", 0, NA)


# asignar CAMOV a paz1
asignOp <- function(fuero, tipo_org, cat, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             categoria == cat &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidades_op$categoria <- NULL
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}


asignOp("Civil", "jdo", 1, "paz", "CAMOV", "mensual", "2017-11-01", 0, NA)
rm(asignOp)

# Rehabilito Función
asignOp <- function(fuero, tipo_org, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades_v3 %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidad_operacion <- bind_rows(entidad_operacion_19_03, entidades_op)
  entidad_operacion_19_03 <<- entidad_operacion
}

# asigno CETAL

asignOp("Civil", "jdo", "cco", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|fam|lab", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "cco|lab|pen", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "fam", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Civil", "jdo", "fam|pen", "CETAL", "anual", "2018-02-01", 0, NA)

asignOp("Civil", "jdo", "paz", "CETAL", "anual", "2018-02-01", 0, NA)

asignOp("Laboral", "jdo", "lab", "CETAL", "anual", "2018-02-01", 0, NA)

asignOp("Civil", "cam", "cco", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Laboral", "cam", "lab", "CETAL", "anual", "2018-02-01", 0, NA)

asignOp("Civil", "sal", "cco", "CETAL", "anual", "2018-02-01", 0, NA)
asignOp("Laboral", "sal", "lab", "CETAL", "anual", "2018-02-01", 0, NA)

# asignar CADRPPC Y "CARCH"  
# oga_pna <- tribble(
#   ~organismo, ~id_operacion, ~periodicidad, ~finicio, ~auxiliares, ~ffin,
#   "ogapen0000pna", "CADRPPC", "mensual", "2017-11-01", 0, NA,
#   "ogapen0000pna", "CARCH", "mensual", "2017-11-01", 0, NA
#   )
# 
# entidad_operacion <- bind_rows(entidad_operacion, oga_pna)
# rm(oga_pna)


# asignar CITIPM, CITIPM2 y CITIPM3

asignOp("Civil", "mui", "cco|fam|paz|lab", "CITIPM", 
        "mensual", "2017-11-01", 0, NA)

asignOp("Civil", "mui", "cco|fam|paz|lab", "CITIPM2", 
        "mensual", "2017-11-01", 0, NA)

asignOp("Civil", "mui", "cco|fam|paz|lab", "CITIPM3", 
        "mensual", "2017-11-01", 0, NA)


# asignar "CITIPP23" y "CADRA23"


# asignar CAMOV a paz1
asignOp <- function(fuero, tipo_org, cat, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             categoria == cat &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidades_op$categoria <- NULL
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

asignOp("Civil", "jdo", 2, "paz", "CITIPP23", "mensual", "2017-11-01", 0, NA)
asignOp("Civil", "jdo", 3, "paz", "CITIPP23", "mensual", "2017-11-01", 0, NA)

asignOp("Civil", "jdo", 2, "paz", "CADRA23", "mensual", "2017-11-01", 1, NA)
asignOp("Civil", "jdo", 3, "paz", "CADRA23", "mensual", "2017-11-01", 1, NA)

# Pendientes ----------------------------------------------------------------

"ADHOC"  
"TIT"

# Modificación: agrego campo de archivadas en cámara
entidad_operacion_09_03$auxiliares[str_detect(entidad_operacion_09_03$id_operacion, 
                                              "CADRIA2")] <- 1



# Operaciones penales Viejo Sistema

#GTIA_VS.R # VER
organismo <- c("ogapen0000con", "ogapen0000gay",
               "ogapen0000gch",
               "ogapen0000lpa",
               "ogapen0000nog",
               "ogapen0000uru",
               "ogapen0000cha",
               "jezpen0000col",
               "jezpen0000dia",
               "jezpen0000fed",
               "jezpen0000frl",
               "jezpen0000fel",
               "jezpen0000tal",
               "jezpen0000vic",
               "jezpen0000vil")
id_operacion <- c("GTIA_VS")
periodicidad <- c("mensual")
finicio <- c("2018-04-01")
auxiliares <- c(10)
ffin <- NA
gtia_VS <- data.frame(organismo, id_operacion, periodicidad, finicio, auxiliares, ffin)
gtia_VS$finicio <- as.character(gtia_VS$finicio)
entidad_operacion_19_03 <- bind_rows(entidad_operacion_19_03, gtia_VS)

#TJUI_VS.R
organismo <- c("ogapen0000con", "ogapen0000gay", "ogapen0000gch",
               "ogapen0000lpa", "ogapen0000nog", "ogapen0000uru")
id_operacion <- c("TJUI_VS")
periodicidad <- c("mensual")
finicio <- c("2018-04-01")
auxiliares <- c(10)
ffin <- NA
TJUI_VS <- data.frame(organismo, id_operacion, periodicidad, finicio, auxiliares, ffin)
TJUI_VS$finicio <- as.character(TJUI_VS$finicio)
entidad_operacion_19_03 <- bind_rows(entidad_operacion_19_03, TJUI_VS)

#TRAN_VS.R

asignOp("Penal", "tra", "pen", "TRAN_VS", "mensual", "2018-04-01", 0, NA)



# GUARDAR df-----------------------------------------------------------------

write.table(entidades, "entidades_v2.csv", col.names = T, row.names = F, sep = ",")
write.table(entidad_operacion_19_03, "entidad_operacion_21_03.csv", 
            col.names = T, row.names = F, sep = ",")


# corrección error en cadria1cm



# asignar CADRIA1C
asignOp("Civil", "jdo", "cco", "CADRIA1C", "mensual", "2017-11-01", 3, NA)
asignOp("Civil", "jdo", "fam", "CADRIA1C", "mensual", "2017-11-01", 3, NA)
asignOp("Civil", "jdo", "fam|pen", "CADRIA1C", "mensual", "2017-11-01", 3, NA)



# rehabilito funcion 
asignOp <- function(fuero, tipo_org, mats, operacion, 
                    periodicidad, finicio, auxiliares, ffin) {
  
  entidades_op <- entidades %>% 
    filter(str_detect(fuero, fuero) &
             tipo == tipo_org &
             materia == mats) %>% 
    select(organismo)
  entidades_op$id_operacion <- operacion
  entidades_op$periodicidad <- periodicidad
  entidades_op$finicio <- finicio
  entidades_op$auxiliares <- auxiliares
  entidades_op$ffin <- ffin
  entidad_operacion <- bind_rows(entidad_operacion, entidades_op)
  entidad_operacion <<- entidad_operacion
}

# asignar CADRIA1CM con 6 campos auxiliares
asignOp("Civil", "jdo", "cco|lab", "CADRIA1CM", "mensual", "2017-11-01", 6, NA)

#------------------------------------------------------------------------------
# Corrección de asignación de materia en entidades a jdocco0000frl
# entidades$materia[entidades$organismo == "jdocco0000frl"] <- "cco|lab"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar CADRIA1CM2 
asignOp("Civil", "jdo", "cco|fam|lab", "CADRIA1CM2", "mensual", "2017-11-01", 9, NA)

#------------------------------------------------------------------------------
# Corrección de asignación de materia en entidades a jdocco0000frl
#entidades$materia[entidades$organismo == "jdopen0000fel"] <- "cco|lab|pen"
#entidades$materia[entidades$organismo == "jdofam0000fel"] <- "fam"
# Corrección de postgresSQL entidades!!!!!!
#------------------------------------------------------------------------------

# asignar CADRIA1CM3
asignOp("Civil", "jdo", "cco|lab|pen", "CADRIA1CM3", "mensual", "2017-11-01", 91, NA)



# ACTUALIZACIÓN 09/04/18
# actualizar histórico cadria1c para los cco|lab y cco|lab|pen con fecha inicio 10/17 y finalización 1/3/18
errorcadria <- entidad_operacion %>% 
  filter(operacion %in% c("CADRIA1CM", "CADRIA1CM2", "CADRIA1CM3"))
errorcadria$operacion <- "CADRIA1C"
errorcadria$finalizacion <- "2018-03-01"
errorcadria$finalizacion <- as.Date(errorcadria$finalizacion)
entidad_operacion <- bind_rows(entidad_operacion, errorcadria)


# eliminar cadria1cm3 de jdopen0000fel
error_jdopen <- entidad_operacion %>% 
  filter(organismo == "jdopen0000fel", operacion == "CADRIA1CM") #%>% 
error_jdopen$operacion <- "CADRIA1C"
error_jdopen$comienzo <- "2017-11-01"
error_jdopen$comienzo <- as.Date(error_jdopen$comienzo)
error_jdopen$auxiliares <- 6
entidad_operacion <- bind_rows(entidad_operacion, error_jdopen)

entidad_operacion$auxiliares[entidad_operacion$organismo == "jdopen0000fel" &
                               entidad_operacion$operacion == "CADRIA1CM"] <- 6

entidad_operacion$finalizacion[entidad_operacion$organismo == "jdopen0000fel" &
                               entidad_operacion$operacion == "CADRIA1CM"] <- NA

entidad_operacion <- entidad_operacion %>%
  filter( operacion != "CADRIA1CM3") 
entidad_operacion <- bind_rows(entidad_operacion, error_jdopen)
entidad_operacion$operacion[entidad_operacion$organismo == "jdopen0000fel" &
                              entidad_operacion$operacion == "CADRIA1C"] <- "CADRIA1CM"
jdopenaudil <- entidad_operacion[196, ]
jdopenaudil$organismo <- "jdopen0000fel"
jdopenaudil$operacion <- "AUDIL"
entidad_operacion <- bind_rows(entidad_operacion, jdopenaudil)

# modificar actual caddria1cm para los cco|lab y cco|lab|pen con fecha inicio 01/04/18 y finalizcion NA
# modificar actual caddria1cm2 para los cco|fam|lab con fecha inicio 01/04/18 y finalizcion NA

entidad_operacion$comienzo[entidad_operacion$operacion %in% c("CADRIA1CM", "CADRIA1CM2")] <- "2018-04-01"



