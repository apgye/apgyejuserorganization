# T2652_InicioReadDB_ogufamGch.R
library(readr)
library(dplyr)

organismo_read <- "ogufam0000gch"
organismo_comienzo <- as.Date("2024-12-01")
finaliza_el <- as.Date("2024-11-30")

setwd("~/apgyeJusEROrganization/inst/apgye")
op_org <- read_csv("entidad_operacion.csv", show_col_types = FALSE)

# buscamos las 4 operaciones a agregar, caso testigo un fam

op_org_agregar <- op_org %>%
  filter(organismo == "jdofam0100gch" & read_db)

op_org_agregar <- op_org_agregar %>%
  mutate(organismo = organismo_read,
         comienzo = organismo_comienzo,
         read_db_desde = organismo_comienzo)

op_org <- op_org %>%
  bind_rows(op_org_agregar)

op_org$finalizacion[op_org$organismo %in% c("jdofam0100gch","jdofam0200gch") & op_org$read_db == TRUE] <- finaliza_el

write.table(op_org, "entidad_operacion.csv", sep = ",", row.names = F, col.names = T)

# agregamos relación en lookupentidades

orgsvec_ver_prod = tbl(DB_PROD(), "lookupentidades") %>%
  collect()

agregar <- data.frame(
  codigo_organismo = c("guafam"),
  organismo = organismo_read
)

orgsvec_ver_prod <- orgsvec_ver_prod %>%
  bind_rows(agregar)

View(orgsvec_ver_prod)
dbWriteTable(DB_PROD(), "lookupentidades", orgsvec_ver_prod, overwrite = TRUE, row.names = FALSE)


org <- read_csv("entidades.csv", show_col_types = FALSE)
View(org)

org_agregar <- data.frame(
  organismo = organismo_read, organismo_descripcion = "OGU Familia",
  email_oficial = "ogu-ggchu@jusentrerios.gov.ar", fuero = "Civil y Comercial",
  circunscripcion = "Gualeguaychú", localidad = "Gualeguaychú", #categoria = NA,
  tipo = "ogu", materia = "fam"
)

org <- org %>%
  bind_rows(org_agregar)

write.table(org, "entidades.csv", sep = ",", row.names = F, col.names = T)
