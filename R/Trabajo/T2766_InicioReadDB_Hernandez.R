# T2766_Hernandez
library(readr)
library(dplyr)

organismo_read <- "jdopaz0000hez"
organismo_comienzo <- as.Date("2025-03-01")
finaliza_el <- as.Date("2025-02-28")
organismo_en_mongo <- "nogherpaz2"

setwd("~/apgyeJusEROrganization/inst/apgye")
op_org <- read_csv("entidad_operacion.csv", show_col_types = FALSE)

op_org$finalizacion[op_org$organismo == organismo_read &
                      op_org$operacion %in% c("CITIPP23", "CADRA23")] <- finaliza_el
# View(op_org)

# buscamos las 4 operaciones a agregar, caso testigo jdopaz0000ibi

op_org_agregar <- op_org %>%
  filter(organismo == "jdopaz0000ibi" & read_db)

op_org_agregar <- op_org_agregar %>%
  mutate(organismo = organismo_read,
         comienzo = organismo_comienzo,
         read_db_desde = organismo_comienzo)

op_org <- op_org %>%
  bind_rows(op_org_agregar)
# View(op_org)

write.table(op_org, "entidad_operacion.csv", sep = ",", row.names = F, col.names = T)

# agreagamos relación en lookupentidades

orgsvec_ver_prod = tbl(DB_PROD(), "lookupentidades") %>%
  # filter(organismo %in% !!orgsvec) %>%
  # select(codigo_organismo) %>%
  collect()
# View(orgsvec_ver_prod)

agregar <- data.frame(
  codigo_organismo = organismo_en_mongo,
  organismo = organismo_read
)

orgsvec_ver_prod <- orgsvec_ver_prod %>%
  bind_rows(agregar)

# View(orgsvec_ver_prod)
dbWriteTable(DB_PROD(), "lookupentidades", orgsvec_ver_prod, overwrite = TRUE, row.names = FALSE)


CHECK_orgsvec_ver_prod = tbl(DB_PROD(), "lookupentidades") %>%
  # filter(organismo %in% !!orgsvec) %>%
  # select(codigo_organismo) %>%
  collect() %>% View()
