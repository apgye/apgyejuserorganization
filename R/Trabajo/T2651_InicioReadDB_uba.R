# T2647_InicioReadDB_Urd.R
library(readr)
library(dplyr)

organismo_read <- "jdopaz0000uba"
organismo_comienzo <- as.Date("2024-11-01")

setwd("~/apgyeJusEROrganization/inst/apgye")
op_org <- read_csv("entidad_operacion.csv", show_col_types = FALSE)

# op_org <- apgyeJusEROrganization::listar_operaciones_por_organismo()

op_org$finalizacion[op_org$organismo == organismo_read &
                      op_org$operacion %in% c("CITIPP23", "CADRA23")] <- "2024-10-31"

# buscamos las 4 operaciones a agregar, caso testigo jdopaz0000ibi

op_org_agregar <- op_org %>%
  filter(organismo == "jdopaz0000ibi" & read_db)

op_org_agregar <- op_org_agregar %>%
  mutate(organismo = organismo_read,
         comienzo = organismo_comienzo,
         read_db_desde = organismo_comienzo)

op_org <- op_org %>%
  bind_rows(op_org_agregar)

write.table(op_org, "entidad_operacion.csv", sep = ",", row.names = F, col.names = T)



