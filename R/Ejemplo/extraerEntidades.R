library(dplyr)
library(readr)
library(stringr)

entidades <- read_csv("inst/apgye/entidades.csv")
#View(entidades)

org_Inactivos <- c("jdofam0101con",
                   "jdofam0102con",
                   "jdofam0201con",
                   "jdofam0202con",
                   "jdofam0001gch",
                   "jdofam0002gch",
                   "jdofam0001uru",
                   "jdofam0002uru")

entResolPG <- entidad_operacion[entidad_operacion$operacion == "RESOLPG",]

entFiltradas <- entidades
entFiltradas %>% left_join(entResolPG, by = "organismo")

final <- entidad_operacion %>%
  filter(operacion == "RESOLPG") %>%
  left_join(entidades, by="organismo")


# Sacamos los orgnismos para informar que se va por Capacitaciones Estadísticas
circ <- c("La Paz", "Feliciano")
ent_F_LP <- entidades %>%
  filter(circunscripcion %in% circ) %>%
  filter(!is.na(tipo) & !tipo == "ein")

write.table(ent_F_LP, "ent_Fel_LaPaz.csv", sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# exportamos Organimos del Fuero Penal - envío de Boletín - 20190423
entFiltradas <- entidades
f_fuero <- "Penal"
f_tipo_excl <- c("del","ein","equ","oma")

entFiltradas <- entFiltradas %>%
  filter(fuero %in% f_fuero) %>%
  filter(!tipo %in% f_tipo_excl) %>%
  filter(!is.na(email_oficial))

write.table(entFiltradas, "Entidades_Penal_20190423.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# exportamos Todos los Organimos - envío a Informática - 20190425
write.table(entFiltradas, "Entidades_20190425.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# exportamos Todos los Organismos Fueros Civil y Laboral - 20190513
entFiltradas <- entidades
f_fuero <- "Penal"
f_tipo_excl <- c("del","ein","equ","oma")

View(entFiltradas)
table(entFiltradas$tipo)

entFiltradas <- entidades %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(!fuero %in% f_fuero) %>%
  filter(!tipo %in% f_tipo_excl) %>%
  filter(!is.na(email_oficial))

write.table(entFiltradas, "Entidades_20190513.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# exportamos las Areas del STJ - 20190520
entFiltradas <- entidades
f_fuero <- "STJ"

entFiltradas <- entidades %>%
  filter(fuero %in% f_fuero) %>%
  filter(!is.na(email_oficial))

View(entFiltradas)

write.table(entFiltradas, "Entidades_Areas_20190520.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# exportamos TODOS los Organimos - envío de Boletín - 20190523
entFiltradas <- entidades

entFiltradas_todas <- entidades %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(!is.na(email_oficial))

View(entFiltradas_todas)

setwd("R/Ejemplo/extracciones/")
write.table(entFiltradas_todas, "Entidades_todas_20190909.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# quitamos las áreas administrativas por el Fuero 'STJ'
entFiltradas_sinSTJ <- entidades %>%
  filter(!fuero == "STJ") %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(!is.na(email_oficial))

View(entFiltradas_sinSTJ)


write.table(entFiltradas_sinSTJ, "Entidades_sinSTJ_20190523.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# Capacitacion Uru y Gchu el 28/5

capacit28May <- entidades %>%
  filter(grepl("Uruguay|Gualeguaych", circunscripcion)) %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(!grepl("oma|equpen", organismo)) %>%
  filter(!is.na(email_oficial))

write.table(capacit28May, "capacit28May_20190527.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# Org Civiles para Oralidad. 20190605
organismos_cyq <- c("jdocco0501con", "jdocco0502con", "jdocco0301uru",
                    "jdocco0302uru", "jdocco0901pna", "jdocco0902pna",
                    "jdocco1001pna", "jdocco1002pna")

oralidad <- entidades %>%
  filter(tipo == "jdo", str_detect(materia, "cco" )) %>%
  filter(!organismo %in% organismos_cyq)

write.table(oralidad, "oralidad_38_organismos.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")
View(oralidad)
# Org Laborales para Oralidad. 20190610
organismos_cyq <- c("jdocco0501con", "jdocco0502con", "jdocco0301uru",
                    "jdocco0302uru", "jdocco0901pna", "jdocco0902pna",
                    "jdocco1001pna", "jdocco1002pna")

oralidad_lab <- entidades %>%
  filter(tipo == "jdo", str_detect(materia, "lab" )) %>%
  filter(!organismo %in% organismos_cyq) %>%
  filter(!organismo %in% org_Inactivos)

write.table(oralidad_lab, "oralidad_laboral.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# Organimos de Familia - 20190910 - Solicitud Relevamiento Adopción
entFiltradas <- entidades

entFiltradas_todas <- entidades %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(grepl("fam", materia)) %>%
  filter(!grepl("mui", organismo)) %>%
  filter(!is.na(email_oficial))

View(entFiltradas_todas)

setwd("R/Ejemplo/extracciones/")
write.table(entFiltradas_todas, "Organismos_Releva_Adopcion_20190910.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# Exportación para Invitación a Evento Publicación Tablero - 20191202
entFiltradas <- entidades

entFiltradas_todas <- entidades %>%
  filter(!organismo %in% org_Inactivos) %>%
  filter(!is.na(email_oficial))

View(entFiltradas_todas)

setwd("R/Ejemplo/extracciones/")
write.table(entFiltradas_todas, "Mails_Inivitacion_20191202.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")

# Exportación para OGAs - Garantías - 20201102
entidades <- listar_organismos()

entFiltradas <- entidades %>%
  filter(!is.na(email_oficial)) %>%
  filter(grepl("jez|oga", tipo),
         grepl("pen", materia))

# preparado de mails para copiar y pegar en Para del Correo
mails <- str_c(entFiltradas$email_oficial, collapse = ", ")
mails
View(entFiltradas_todas)

setwd("R/Ejemplo/extracciones/")
write.table(entFiltradas_todas, "Mails_Inivitacion_20191202.csv",
            sep = ",", col.names = TRUE, fileEncoding = "ISO-8859-1")


# exportamos Todos los Organismos Fueros Civil 1era Instancia - 20221227
entFiltradas <- entidades %>%
  filter(!organismo %in% org_Inactivos) %>%
  #filter(grepl("cco|fam|paz|ecq", organismo) &
  filter(fuero == "Civil y Comercial" &
           tipo == "jdo") %>%
  filter(!is.na(email_oficial))

View(entFiltradas)
table(entFiltradas$materia)

# preparado de mails para copiar y pegar en Para del Correo
mails <- str_c(entFiltradas$email_oficial, collapse = ", ")
mails

# exportamos las Salas Civiles - 20221228
entFiltradas <- entidades %>%
  # filter(!organismo %in% org_Inactivos) %>%
  #filter(grepl("cco|fam|paz|ecq", organismo) &
  filter(fuero == "Civil y Comercial" &
           tipo == "cam") %>%
  filter(!is.na(email_oficial))

View(entFiltradas)
table(entFiltradas$materia)

# preparado de mails para copiar y pegar en Para del Correo
mails_cam <- str_c(entFiltradas$email_oficial, collapse = ", ")
mails_cam



# para Taiga 2295 extraccion de mails para comunicación
library(dplyr)
library(stringr)
#entidades <- apgyeJusEROrganization::listar_organismos()

apgyeRStudio::listar_presentaciones(DB_PROD(), v = F)
ent_present <- presentaciones_justat
entFiltradas <- ent_present %>%
  filter(!is.na(oficial_email) |
           !oficial_email == "" | 
           !oficial_email == " " |
           grepl("jusentrerios.gov.ar", oficial_email, fixed = T))

View(entFiltradas)
mails_entidades <- unique(entFiltradas$oficial_email)

View(entFiltradas)
  



