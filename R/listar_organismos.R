library(data.table)
flog.debug("loading ieps")
ALL_ENTITIES <- readr::read_csv(system.file("apgye", "entidades.csv", package = "apgyeJusEROrganization"))
flog.debug("loading submissions")
ALL_SUBMISSIONS_BY_IEP <- readr::read_csv(system.file("apgye", "entidad_operacion.csv", package = "apgyeJusEROrganization"))
ALL_SUBMISSIONS_BY_IEP$operacion_descripcion <- NULL
OPERATION_DESCRIPTION <- readr::read_csv(system.file("apgye", "operacion_descripcion.csv", package = "apgyeJusEROrganization"))
ALL_SUBMISSIONS_BY_IEP <- ALL_SUBMISSIONS_BY_IEP %>%
  left_join(OPERATION_DESCRIPTION)

flog.debug("done loading ieps and submissions")

listar_organismos <- function() {
  ALL_ENTITIES
}


listar_operaciones_por_organismo <- function() {
  # modificaciones sobre entidad_operacion
  # ALL_SUBMISSIONS_BY_IEP <- ALL_SUBMISSIONS_BY_IEP %>%
  # mutate(fvencimiento = 11) %>% mutate(mvencimiento = ifelse(startsWith(operacion,"CADUR"), 10 , ifelse(startsWith(operacion,"CETAL"), 2, NA)))  %>% left_join(op_desc)
  ALL_SUBMISSIONS_BY_IEP
}




periodsByOperation <- function(organismo, operacion) {
  if(organismo != '' & operacion != '') {
    frecuencia <- ALL_SUBMISSIONS_BY_IEP %>%
      filter(organismo == !!organismo,
             operacion == !!operacion,
             is.na(finalizacion)) %>%
      .$frecuencia

    if (length(frecuencia) != 1){
      return(data.frame(periodo_descripcion=c("--"), periodo=c(NA)))
    }
    switch(frecuencia,
           "mensual" = {
             data.frame(periodo_descripcion=lubridate::month(seq(12), label = TRUE, abbr = FALSE), periodo=seq(12))
           },
           "semestral" = {
             data.frame(periodo_descripcion=c("primer semestre", "segundo semestre"), periodo=c(13,14))
           },
           "anual" = {
             data.frame(periodo_descripcion=c("anual"), periodo=c(15))
           }
    )

  }
}

